// DANIEL GARC�A �VILA - EJERCICIO03
public class EJERCICIO03 {

	public static void main(String[] args) {
		
		int x = 200, y = 100;
		double n = 25.5, m = 50.5;
		int suma1 = x + y;
		int resta1 = x - y;
		int multiplicaci�n1 = x * y;
		int divisi�n1 = x / y;
		int m�dulo1 = x % y;
		double suma2 = n + m;
		double resta2 = n - m ;
		double multiplicaci�n2 = n + m;
		double divisi�n2 = n / m;
		double m�dulo2 = n % m;
		int doblex = x * 2;
		int dobley = y * 2;
		double doblen = n * 2;
		double doblem = m * 2;
		double sumatodas = x + y + n + m;
		double producto = x * y * n * m;
		
		System.out.println("El valor de 'x' es igual a: "+x);
		System.out.println("El valor de 'y' es igual a: "+y);
		System.out.println("El valor de 'n' es igual a: "+n);
		System.out.println("El valor de 'm' es igual a: "+m);
		System.out.println("La suma de 'x' + 'y' es igual a: "+suma1);
		System.out.println("La diferencia de 'x' - 'y' es igual a: "+resta1);
		System.out.println("El producto de 'x' * 'y' es igual a: "+multiplicaci�n1);
		System.out.println("El cociente de 'x' / 'y' es igual a: "+divisi�n1);
		System.out.println("El resto de 'x' % 'y' es igual a: "+m�dulo1);
		System.out.println("La suma de 'n' + 'm' es igual a: "+suma2);
		System.out.println("La diferencia de 'n' - 'm' es igual a: "+resta2);
		System.out.println("El producto de 'n' * 'm' es igual a: "+multiplicaci�n2);
		System.out.println("El cociente de 'n' / 'm' es igual a: "+divisi�n2);
		System.out.println("El resto de 'n' % 'm' es igual a: "+m�dulo2);
		System.out.println("El doble de 'x' es igual a: "+doblex);
		System.out.println("El doble de 'y' es igual a: "+dobley);
		System.out.println("El doble de 'n' es igual a: "+doblen);
		System.out.println("El doble de 'm' es igual a: "+doblem);
		System.out.println("La suma de todas las variables es igual a: "+sumatodas);
		System.out.println("El producto de todas las variables es igual a: "+producto);
		
	}

}
