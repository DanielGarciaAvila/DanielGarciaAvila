// DANIEL GARC�A �VILA - EJERCICIO02
public class EJERCICIO02 {

	public static void main(String[] args) {
		
		int n = 100;
		double a = 250.5;
		char c = 50;
		double suma = n + a;
		double resta = a - n;
		
				
		System.out.println("El valor de 'n' es igual a: "+n);
		System.out.println("El valor de 'a' es igual a: "+a);
		System.out.println("El valor de 'c' es igual a: "+c);
		
		System.out.println("La suma de 'n' + 'a' es igual a: "+suma);
		System.out.println("La resta de 'a' - 'n' es igual a: "+resta);
		System.out.println("El valor num�rico de la variable 'c' es igual a: "+c);
		
	}

}
