// DANIEL GARC�A �VILA - EJERCICIO16
import javax.swing.JOptionPane;
public class EJERCICIO16 {

	public static void main(String[] args) {
		
		String semana = JOptionPane.showInputDialog("Introduce un d�a de la semana: ");
		
		switch (semana) {
			case "Lunes":
				JOptionPane.showMessageDialog(null,"El lunes es d�a laboral.");
				break;
			case "Martes":
				JOptionPane.showMessageDialog(null,"El martes es d�a laboral.");
				break;
			case "Miercoles":
				JOptionPane.showMessageDialog(null,"El mi�rcoles es d�a laboral.");
				break;
			case "Jueves":
				JOptionPane.showMessageDialog(null,"El jueves es d�a laboral.");
				break;
			case "Viernes":
				JOptionPane.showMessageDialog(null,"El viernes es d�a laboral.");
				break;
			case "Sabado":
				JOptionPane.showMessageDialog(null,"El s�bado no es d�a laboral.");
				break;
			case "Domingo":
				JOptionPane.showMessageDialog(null,"El domingo no es d�a laboral.");
				break;
			default:
				JOptionPane.showMessageDialog(null,"El d�a que has introducido no existe. Prueba a poner la primera letra en may�scula.");
		}

	}

}
