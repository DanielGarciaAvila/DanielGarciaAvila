// DANIEL GARC�A �VILA - EJERCICIO15
import javax.swing.JOptionPane;
public class EJERCICIO15 {

	public static void main(String[] args) {
	
		String numero = JOptionPane.showInputDialog("Introduce un n�mero de ventas: ");
		int introducido = Integer.parseInt(numero);
		int total = 0;
		
		for (int ventas = 1; ventas <= introducido; ventas++) {
			String cantidad = JOptionPane.showInputDialog("Cantidad de ventas: ");
			total = total + Integer.parseInt(cantidad);
		}
		JOptionPane.showMessageDialog(null,"La suma total de las ventas es: "+total);
	}

}