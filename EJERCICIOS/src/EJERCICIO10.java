// DANIEL GARC�A �VILA - EJERCICIO10
import javax.swing.JOptionPane;
public class EJERCICIO10 {

	public static void main(String[] args) {
		
		String numero = JOptionPane.showInputDialog("Introduce un n�mero: ");
		double num = Double.parseDouble (numero);
		
		if ((num / 2)==0){
			JOptionPane.showMessageDialog(null, "El n�mero introducido es divisible entre dos");
		}
		if ((num / 2)!=0){
			JOptionPane.showMessageDialog(null, "El n�mero introducido no es divisible entre dos");
		}
		
	}

}
