// DANIEL GARC�A �VILA - EJERCICIO18
import javax.swing.JOptionPane;
public class EJERCICIO18 {

	public static void main(String[] args) {
		
		String num1 = JOptionPane.showInputDialog("Introduce un n�mero :");
		String num2 = JOptionPane.showInputDialog("Introduce otro n�mero :");
		String signo = JOptionPane.showInputDialog("Introduce un signo aritm�tico :");
		int numero1 = Integer.parseInt(num1);
		int numero2 = Integer.parseInt(num2);
		
		
		switch (signo) {
			case "+":
				JOptionPane.showMessageDialog(null,"La suma de "+numero1+" mas "+numero2+" es : "+(numero1 + numero2));
				break;
			case "-":
				JOptionPane.showMessageDialog(null, "La resta de "+numero1+" menos "+numero2+" es : "+(numero1 - numero2));
				break;
			case "*":
				JOptionPane.showMessageDialog(null, "La multiplicaci�n de "+numero1+" por "+numero2+" es : "+(numero1 * numero2));
				break;
			case "/":
				JOptionPane.showMessageDialog(null, "La divisi�n de "+numero1+" entre "+numero2+" es : "+(numero1 / numero2));
				break;
			case "^":
				JOptionPane.showMessageDialog(null, "Los operandos de "+numero1+" y "+numero2+" es : "+(numero1 ^ numero2));
				break;
			case "%":
				JOptionPane.showMessageDialog(null, "El m�dulo de la divisi�n de "+numero1+" entre "+numero2+" es : "+(numero1 % numero2));
				break;
		}
	}

}
