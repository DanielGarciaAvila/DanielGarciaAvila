// DANIEL GARC�A �VILA - EJERCICIO05
public class EJERCICIO05 {

	public static void main(String[] args) {
		
		int a = 5, b = 10, c = 15, d = 20;
		int valorc = c;
		int valora = a;
		int valord = d;
		int valorb = b;
		
		System.out.println("La variable 'b' toma el valor de la variable 'c': "+valorc);
		System.out.println("La variable 'c' toma el valor de la variable 'a': "+valora);
		System.out.println("La variable 'a' toma el valor de la variable 'd': "+valord);
		System.out.println("La variable 'd' toma el valor de la variable 'b': "+valorb);
		
	}

}
