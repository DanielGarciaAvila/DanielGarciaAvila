// DANIEL GARC�A �VILA - EJERCICIO09
import javax.swing.JOptionPane;
public class EJERCICIO09 {

	public static void main(String[] args) {
		
		final double PI = 3.1416;
		
		String circulo = JOptionPane.showInputDialog("Introduce el radio de un c�rculo: ");
		double radio = Double.parseDouble (circulo);
		double radio2 = Math.pow(radio, 2);
		double total = radio2 * PI;
		
		JOptionPane.showMessageDialog(null, "El �rea del c�rculo es: "+total);
		
	}

}
