// DANIEL GARC�A �VILA - EJERCICIO04
public class EJERCICIO04 {

	public static void main(String[] args) {
		
		int n =  100;
		int incrementar = n + 77;
		int decrementar = incrementar - 3;
		int duplicar = decrementar * 2;
		
		System.out.println("El valor de 'n' es igual a: "+n);
		System.out.println("El valor de 'n' se incrementa en 77: "+incrementar);
		System.out.println("El valor de 'n' ahora se decrementa en 3: "+decrementar);
		System.out.println("Por �ltimo, la variable 'n' duplica su valor: "+duplicar);
		
	}

}
