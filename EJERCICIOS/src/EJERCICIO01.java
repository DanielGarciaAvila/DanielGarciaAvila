// DANIEL GARC�A �VILA - EJERCICIO01
public class EJERCICIO01 {

	public static void main(String[] args) {
		
		int a = 50, b = 10;
		int suma = a + b;
		int resta = a - b;
		int multiplicaci�n = a * b;
		int divisi�n = a / b;
		int m�dulo = a % b;
		
		System.out.println ("El resultado de la suma de a + b es: " +suma);
		System.out.println ("El resultado de la resta de a - b es: " +resta);
		System.out.println ("El resultado de la multiplicaci�n de a * b es: " +multiplicaci�n);
		System.out.println ("El resultado de la divisi�n de a / b es: " +divisi�n);
		System.out.println ("El resultado del m�dulo de a % b es: " +m�dulo);

	}

}
