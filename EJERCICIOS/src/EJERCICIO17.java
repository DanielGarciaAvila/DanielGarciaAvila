// DANIEL GARC�A �VILA - EJERCICIO17
import javax.swing.JOptionPane;
public class EJERCICIO17 {

	public static void main(String[] args) {
	
		String pass = "testers";
		String contrase�a;
		boolean aceptado = false;
		
		int intentos = 3;
		
		do {
			contrase�a = JOptionPane.showInputDialog("Introduce la contrase�a :");
			
			if (contrase�a.equals(pass)) {
				
				aceptado = true;
				JOptionPane.showMessageDialog(null, "Contrase�a correcta.");
				
			} else {
				
				intentos --;
				JOptionPane.showMessageDialog(null, "Contrase�a incorrecta. "+intentos+" intento(s) restantes.");
			}
			
		} while(aceptado == false && intentos > 0);
		
}
}
